// Source code for this component:
// https://stackoverflow.com/questions/63591022/how-to-calculate-time-on-a-specific-screen-user-spend-in-react-native

import React, { useState, useEffect } from "react";

export const Timer = (props) => {
  const [time, setTime] = useState({
    seconds: 0,
    minutes: 0,
    hours: 0,
  });

  useEffect(() => {
    let isCancelled = false;

    const advanceTime = () => {
      setTimeout(() => {
        let nSeconds = time.seconds;
        let nMinutes = time.minutes;
        let nHours = time.hours;

        nSeconds++;

        if (nSeconds > 59) {
          nMinutes++;
          nSeconds = 0;
        }
        if (nMinutes > 59) {
          nHours++;
          nMinutes = 0;
        }
        if (nHours > 24) {
          nHours = 0;
        }

        !isCancelled &&
          setTime({ seconds: nSeconds, minutes: nMinutes, hours: nHours });
      }, 1000);
    };
    advanceTime();

    return () => {
      //final time:
      isCancelled = true;
    };
  }, [time]);

  useEffect(() => {
    props.setTimeSpent(time);
  });

  return <></>;
};

export default Timer;
