import { ButtonGroup, Button, Form } from "react-bootstrap";
import { useState } from "react";

const NextButton = (props) => {
  return (
    <Button
      onClick={() => props.onClick()}
      disabled={props.disabled}
      variant="info"
      style={{
        display: "flex",
        margin: "auto",
        width: "60px",
        marginTop: "25px",
      }}
    >
      Next
    </Button>
  );
};

const RatingButtons = (props) => {
  const inputMode = props.inputMode;
  const inputGranularity = props.inputGranularity;
  const buttonColor = "info";
  const firstButtonColor = "success"
  const lastButtonColor = "danger"
  const [isQuestionAnswered, setIsQuestionAnswered] = useState(false);
  const [answerValue, setAnswerValue] = useState(null);

  const handleSetAnswerValue = (e) => {
    setAnswerValue(e.currentTarget.innerHTML);
    props.setRatingGiven(Number(e.currentTarget.innerHTML));
  };

  const handleSetSliderValue = (v) => {
    props.setRatingGiven(Number(v));
  };

  const onNextClick = () => {
    setIsQuestionAnswered(false);
    setAnswerValue(null);
    props.onNextClick();
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
  };

  const renderChosenSliderValue = (input) => {
    if (input) {
      return input;
    }
    if (answerValue != null) {
      handleSetSliderValue(answerValue);
      return `${answerValue} out of 10`;
    }
  };

  if (inputMode !== "slider") {
    return (
      <>
        <p style={{ textAlign: "center", fontWeight: "bold", marginTop: "45px" }}>
          On a scale from 1 to 10, how bad is this recipes environmental impact?
        </p>
        <p style={{textAlign: "center", color: "darkgray"} }>(not necessarily per portion, but the whole recipe)</p>
        <ButtonGroup
          aria-label="Basic example"
          style={{ display: "flex" }}
          onClick={(e) => {
            setIsQuestionAnswered(true);
          }}
        >
          <Button
            variant={firstButtonColor}
            onClick={(e) => {
              handleSetAnswerValue(e);
            }}
          >
            1
          </Button>
          <Button
            variant={buttonColor}
            onClick={(e) => {
              handleSetAnswerValue(e);
            }}
          >
            2
          </Button>
          <Button
            variant={buttonColor}
            onClick={(e) => {
              handleSetAnswerValue(e);
            }}
          >
            3
          </Button>
          <Button
            variant={buttonColor}
            onClick={(e) => {
              handleSetAnswerValue(e);
            }}
          >
            4
          </Button>
          <Button
            variant={buttonColor}
            onClick={(e) => {
              handleSetAnswerValue(e);
            }}
          >
            5
          </Button>
          <Button
            variant={buttonColor}
            onClick={(e) => {
              handleSetAnswerValue(e);
            }}
          >
            6
          </Button>
          <Button
            variant={buttonColor}
            onClick={(e) => {
              handleSetAnswerValue(e);
            }}
          >
            7
          </Button>
          <Button
            variant={buttonColor}
            onClick={(e) => {
              handleSetAnswerValue(e);
            }}
          >
            8
          </Button>
          <Button
            variant={buttonColor}
            onClick={(e) => {
              handleSetAnswerValue(e);
            }}
          >
            9
          </Button>
          <Button
            variant={lastButtonColor}
            onClick={(e) => {
              handleSetAnswerValue(e);
            }}
          >
            10
          </Button>
        </ButtonGroup>
        <p style={{display: "flex", float: "left", margin: "5px", fontSize: "70%"}}>👍 Low impact  </p>
            <p style={{display: "flex", margin: "5px", float: "right", marginBottom: "15px", fontSize: "70%"}}> High Impact 👎</p> 

        <NextButton disabled={!isQuestionAnswered} onClick={onNextClick} />
      </>
    );
  }
  if (inputMode === "slider") {
    return (
      <>
        <Form>
          <Form.Group controlId="formBasicRange">
          <p style={{ textAlign: "center", fontWeight: "bold", marginTop: "45px" }}>
          On a scale from 1 to 10, how bad is this recipe's impact on the climate?
        </p>
        <p style={{textAlign: "center", color: "darkgray"} }>(not necessarily per portion, but the whole recipe)</p>

            <Form.Control
              type="range"
              min="1"
              max="100"
              onChange={(e) => {
                setAnswerValue(Math.ceil(e.currentTarget.value) / 10);
                setIsQuestionAnswered(true);
              }}
            />
            <p style={{display: "flex", float: "left", margin: "5px", fontSize: "70%"}}>👍 Low impact  </p>
            <p style={{display: "flex", margin: "5px", float: "right", marginBottom: "15px", fontSize: "70%"}}> High Impact 👎</p>
            <p style={{ textAlign: "center", marginTop: "15px" }}>
              {renderChosenSliderValue()}
            </p>


          </Form.Group>
          <NextButton disabled={!isQuestionAnswered} onClick={onNextClick} />

        </Form>
      </>
    );
  }
};

export default RatingButtons;
