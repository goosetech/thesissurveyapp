import { ButtonGroup, ToggleButton } from "react-bootstrap";
import { useState } from "react";

function RadioButtons(props) {
  const [radioValue, setRadioValue] = useState("N/A");


  const handleChange = (e) => {
    props.setter(e.currentTarget.name);
    setRadioValue(e.currentTarget.value);
  }

  return (
    <>
      <br />
      <ButtonGroup toggle>
        {props.options.map((radio, idx) => (
          <ToggleButton
            key={idx}
            type="radio"
            variant="secondary"
            name={radio.name}
            value={radio.value}
            checked={radioValue === radio.value}
            onChange={(e) => handleChange(e)}
          >
            {radio.name}
          </ToggleButton>
        ))}
      </ButtonGroup>
    </>
  );
}

export default RadioButtons;
