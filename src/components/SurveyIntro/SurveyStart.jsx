import { Button, Card } from "react-bootstrap";
import { useState } from "react";

const SurveyStart = (props) => {
  const [introStep, setIntroStep] = useState(1);

  if (introStep === 1) {
    return (
      <Card
        style={{
          width: "min(900px, 90vw)",
          margin: "auto",
          marginTop: "4vw",
        }}
      >
        <Card.Header>The Sustainability of Online Recipes</Card.Header>
        <Card.Body>
          <Card.Title>About The Study 🍔</Card.Title>
          <Card.Text>
            You have been invited to participate in a research study about
            people's perception of online recipes in terms of their
            environmental impact. The study involves a simple questionnaire,
            that shouldn't take more than 5-10 minutes to complete.
          </Card.Text>

          <Card.Title>Rights 👍</Card.Title>
          <Card.Text>
            Participation is 100% voluntary, and you can exit the questionnaire
            at any time should you desire to do so. Questions related to
            yourself as an individual are also optional. None of your answers
            will be stored if you decide to leave the questionnaire
            preemptively.
          </Card.Text>

          <Card.Title>Data 📈</Card.Title>
          <Card.Text>
            {" "}
            The data collected through this survey is anonymous and will be used
            in parts of my master thesis about calculating the sustainability of
            online recipes. Only people directly involved in the study will have
            access to this data until the thesis is finished.
          </Card.Text>

          <Button variant="info" onClick={() => setIntroStep(2)}>
            About The Survey
          </Button>
        </Card.Body>
      </Card>
    );
  } else {
    return (
      <Card
        style={{
          width: "min(1000px, 90vw)",
          margin: "auto",
          marginTop: "4vw",
        }}
      >
        <Card.Header>The Sustainability of Online Recipes</Card.Header>
        <Card.Body>
          <Card.Title>Tasks 📝</Card.Title>
          <Card.Text>
            {" "}
            You will be presented with a total of ten recipes, and will be asked to rate how
            sustainable you <i>think</i> they are on a scale from 1 to 10, just by looking at the ingredients. After rating 10 recipes, you will be asked to provide
            simple demographic information.
          </Card.Text>

          <Card.Title>What do you mean by "sustainable?" 🌱 </Card.Title>
          <Card.Text>
            {" "}
            Each ingredient's sustainability score has been calculated using the <a href="https://www.sciencedirect.com/science/article/pii/S2352340919309722">SHARP-ID</a> dataset. The sum of all ingredient scores, divided by the number of ingredients in a recipe will produce that recipe's sustainability score. The score represents the amount of greenhouse gass emissions and land usage required to produce 1kg of that food.
          </Card.Text>

          <Card.Title>Example scores </Card.Title>
          <Card.Text>
            {" "}

            <a target="_blank" href="http://allrecipes.com/recipe/deliciously-sweet-salad-with-maple-nuts-seeds-blueberries-and-goat-cheese/detail.aspx
" rel="noreferrer">This</a> recipe has a sustainability score of <span style={{color: "green"}}>2.04</span>, which means it's relatively good for our environment.  <br></br>
            While <a target="_blank" href="http://allrecipes.com/recipe/sour-cream-marinated-chicken-ii/detail.aspx" rel="noreferrer">this</a> recipe has a sustainability score of <span style={{color: "red" }}>8.02</span> which means that it's less good for the environment.
            <br></br> <br></br>
            Take a moment to check out the two recipes, in order to gain some context before rating other recipes.
          </Card.Text>
          <hr></hr>

          <Card.Title>Who Can Participate? 🤔</Card.Title>
          <Card.Text>
            Anyone over the age of 18 and with an internet connection can participate in this
            questionnaire.
          </Card.Text>

          <Card.Title>Contact 📬</Card.Title>
          <Card.Text>
            {" "}
            Any inquiries can be directed to Arien Shibani, University of
            Bergen:{" "}
            <a href="mailto: Arien.Shibani@student.uib.no">
              Arien.Shibani@student.uib.no
            </a>
          </Card.Text>
          <Button variant="info" onClick={() => props.onClick("SurveyStart")}>
            Click Here to Start!
          </Button>
        </Card.Body>
      </Card>
    );
  }
};

export default SurveyStart;
