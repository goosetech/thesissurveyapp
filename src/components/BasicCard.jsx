import { Card, Image, ListGroup, ProgressBar, Button } from "react-bootstrap";
//import axios from "axios";
import RatingButtons from "./RatingButtons/RatingButtons";
import RadioButtons from "./RadioButtons/RadioButtons";
import { useState } from "react";

const BasicQuestionCard = (props) => {
  const [recipeNr, setRecipeNr] = useState(1);
  const [recipe, setRecipe] = useState(props.recipes[0]);
  const [currentRecipesRating, setCurrentRecipesRating] = useState(null);

  const [gender, setGender] = useState(1);
  const [ageRange, setAgeRange] = useState(1);
  const [environmentalConcern, setEnvironmentalConcern] = useState(1);
  const [diet, setDiet] = useState(1);

  let demographics = {
    "gender": gender,
    "diet": diet,
    "environmentalConcern": environmentalConcern,
    "ageRange": ageRange
  }

  const setNextRecipe = () => {
    if (props.recipes.length > recipeNr) {
      setRecipeNr(recipeNr + 1);
      setRecipe(props.recipes[recipeNr]);

      //Send the recipe shown, and the rating it recieved to parent state.
      props.setUsersRatingInput({
        rating: currentRecipesRating,
        recipe: recipe,
      });
    }
  };


  //Generate HTML list element for each recipe string.
  const listIngredientStrings = recipe.recipeIngredients.map(
    (ingredient, index) => (
      <ListGroup.Item key={index.toString()}>{ingredient}</ListGroup.Item>
    )
  );

  //Radio buttons for the demographic data collection.
  const genders = [
    { name: "Male", value: "5" },
    { name: "Female", value: "2" },
    { name: "Non-Binary", value: "3" },
    { name: "Transgender", value: "4" },
    { name: "Not Listed", value: "1" },
  ];

  const ageRanges = [
    { name: "18-25", value: "5" },
    { name: "26-30", value: "2" },
    { name: "30-45", value: "3" },
    { name: "45+", value: "4" },
    { name: "Prefer Not To Answer", value: "1" },
  ];

  const environmentConcern = [
    { name: "None at all", value: "5" },
    { name: "To a small degree", value: "2" },
    { name: "Yes.", value: "3" },
    { name: "Very much so", value: "4" },
    { name: "Prefer Not To Answer", value: "1" },
  ];

  const diets = [
    { name: "Vegan", value: "5" },
    { name: "Vegetarian", value: "2" },
    { name: "Pescitarian", value: "3" },
    { name: "None of the above", value: "4" },
    { name: "Prefer Not To Answer", value: "1" },
  ];

  const handleEndSurvey = () => {
    props.setUsersDemographicData(demographics);
    if(demographics){
      props.onClick("Fin");
    } else {
      console.log(props)
    }
  }

  if (recipeNr === props.recipes.length) {
    return (
      <>
        <Card
          style={{
            width: "min(100%, 800px)",
            margin: "auto",
            marginTop: "2vw",
            marginBottom: "5vw",
          }}
        >
          <Card.Body>
            <ProgressBar
              variant="info"
              style={{ marginBottom: "10px" }}
              now={(recipeNr / props.recipes.length) * 100} //progress bar percentage.
            />
            <p>
              Thank you for providing your input on these recipes. The last part
              of this questionnaire you will be asked to answer a few questions
              about yourself before submiting the ratings.
            </p>
            <b>1. What Gender Do you Identify as?</b>
            <RadioButtons options={genders} setter={setGender}></RadioButtons>
            <br />
            <b>2. What is your age?</b>
            <RadioButtons options={ageRanges} setter={setAgeRange}></RadioButtons>
            <br />
            <b>3. Are you highly concerned about the environment?</b>
            <RadioButtons options={environmentConcern} setter={setEnvironmentalConcern}></RadioButtons>
            <br />
            <b>4. Are you on any of these diets?</b>
            <RadioButtons options={diets} setter={setDiet}></RadioButtons>
            <br />
            <Card.Text>
              <Button
                style={{ marginTop: "20px" }}
                variant="info"
                onClick={() => handleEndSurvey()}
              >
                Finish the survey!
              </Button>
            </Card.Text>
          </Card.Body>
        </Card>
      </>
    );
  } else {
    return (
      <>
        <Card
          style={{
            border: "none",
            width: "min(100%, 625px)",
            margin: "auto",
            marginTop: "2vw",
            marginBottom: "5vw",
          }}
        >
          <Card.Body>
            <ProgressBar
              variant="info"
              style={{ marginBottom: "10px" }}
              now={(recipeNr / props.recipes.length) * 100} //progress bar percentage.
            />
            <Card.Title style={{ fontWeight: "bold" }}>
              {recipe.recipeTitle}
            </Card.Title>
            <Image
              style={{
                width: "min(90vw, 585px)",
                height: "25vh",
                objectFit: "cover",
                border: "5px solid #ececec",
                display: "flex",
                margin: "auto",
              }}
              src={recipe.recipeImage}
            />

            <Card.Text>
              <ListGroup style={{ paddingTop: "10px" }}>
                <p>
                  Take a moment to carefully look at the following ingredients, and the amount of ingredients that this recipe has. You will be asked to rate this specific recipe, in terms of how sustainable you percieve it to be.
                </p>

                <b style={{ marginBottom: "10px" }}>
                  Contains these ingredients:
                </b>

                {listIngredientStrings}
              </ListGroup>
              <RatingButtons
                onNextClick={setNextRecipe}
                setRatingGiven={(value) => setCurrentRecipesRating(value)}
                inputMode={props.inputMode}
                inputGranularity={props.inputGranularity}
              />
            </Card.Text>
          </Card.Body>
        </Card>
      </>
    );
  }
};

export default BasicQuestionCard;
