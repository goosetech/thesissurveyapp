import "bootstrap/dist/css/bootstrap.min.css";
import { useState, useEffect } from "react";
import Confetti from "react-confetti"; //Cute confetti animation upon completing the survey.
import { Timer } from "./components/Timer/Timer.jsx";
import { v4 as uuidv4 } from "uuid"; //Generate unique identifiers
import axios from "axios";
import BasicQuestionCard from "./components/BasicCard";
import SurveyStart from "./components/SurveyIntro/SurveyStart";
import { Button } from "react-bootstrap";

function App() {

  const selectedRecipesForSurvey = [
    {
      recipeTitle: 'Bacon Chicken I',
      recipeUrl: 'http://allrecipes.com/recipe/bacon-chicken-i/detail.aspx',
      recipeImage: 'https://imagesvc.meredithcorp.io/v3/mm/image?url=https%3A%2F%2Fimages.media-allrecipes.com%2Fuserphotos%2F4560483.jpg&w=595&h=595&c=sc&poi=face&q=85',
      recipeIngredients: [
        '6  skinless, boneless chicken breast halves',
        '6 slices bacon',
        '1 (10.75 ounce) can  condensed cream of celery soup',
        '¼ cup milk'
      ],
      sustainabilityScore: 7.74605352544008,
      accuracy: 100
    },
    {
      recipeTitle: 'Dole Whip',
      recipeUrl: 'http://allrecipes.com/recipe/dole-whip/detail.aspx',
      recipeImage: 'https://images.media-allrecipes.com/userphotos/3398172.jpg',
      recipeIngredients: [
        '2 (20 ounce) cans crushed pineapple, drained',
        '⅓ cup white sugar',
        '2 tablespoons lemon juice',
        '2 tablespoons lime juice',
        '1 ½ cups heavy whipping cream'
      ],
      sustainabilityScore: 3.3341937037227654,
      accuracy: 100
    },
    {
      recipeTitle: '4H Banana Bread',
      recipeUrl: 'http://allrecipes.com/recipe/4h-banana-bread/detail.aspx',
      recipeImage: 'https://images.media-allrecipes.com/userphotos/8856616.jpg',
      recipeIngredients: [
        '2 cups all-purpose flour',
        '½ teaspoon baking soda',
        '1 cup white sugar',
        '1  egg',
        '5 tablespoons milk',
        '1 teaspoon baking powder',
        '½ teaspoon salt',
        '½ cup margarine',
        '1 cup mashed bananas',
        '½ cup chopped walnuts  (Optional)'
      ],
      sustainabilityScore: 3.3965230004135596,
      accuracy: 90
    },
    {
      recipeTitle: 'Really Truly Gorgeous Dried Fruit Salad',
      recipeUrl: 'http://allrecipes.com/recipe/really-truly-gorgeous-dried-fruit-salad/detail.aspx',
      recipeImage: 'https://images.media-allrecipes.com/userphotos/1011392.jpg',
      recipeIngredients: [
        '8 ounces dried figs',
        '8 ounces dried apricots',
        '8 ounces pitted prunes',
        '½ cup dried cranberries',
        '½ cup raisins',
        '½ cup golden raisins',
        '¼ cup pine nuts',
        '1 tablespoon honey'
      ],
      sustainabilityScore: 0.9123277310087405,
      accuracy: 100
    },
    {
      recipeTitle: 'Laura Shirks Shortbread',
      recipeUrl: 'http://allrecipes.com/recipe/laura-shirks-shortbread/detail.aspx',
      recipeImage: 'https://images.media-allrecipes.com/userphotos/1040186.jpg',
      recipeIngredients: [
        '1 cup butter',
        '½ cup white sugar',
        '2 ⅓ cups all-purpose flour',
        '¼ teaspoon salt'
      ],
      sustainabilityScore: 7.771125176360887,
      accuracy: 100
    },
    {
      recipeTitle: 'Sweetheart Cupcakes',
      recipeUrl: 'http://allrecipes.com/recipe/sweetheart-cupcakes/detail.aspx',
      recipeImage: 'https://images.media-allrecipes.com/userphotos/855523.jpg',
      recipeIngredients: [
        '1 (18.25 ounce) package white cake mix',
        '1 ¼ cups water',
        '⅓ cup vegetable oil',
        '3  egg whites',
        '8 drops red food coloring',
        '2 drops raspberry candy oil'
      ],
      sustainabilityScore: 2.973972533456505,
      accuracy: 100
    },
    {
      recipeTitle: 'Carrot Rice Loaf',
      recipeUrl: 'http://allrecipes.com/recipe/carrot-rice-loaf/detail.aspx',
      recipeImage: 'https://images.media-allrecipes.com/userphotos/1003245.jpg',
      recipeIngredients: [
        '2 cups water',
        '1 cup uncooked brown rice',
        '½ cup chopped onion',
        '2 cups finely grated carrots',
        '¼ cup peanut butter',
        '3  slices whole wheat bread, crumbled',
        '2 cups milk',
        'salt and pepper to taste'
      ],
      sustainabilityScore: 1.521652390612097,
      accuracy: 100
    },
    {
      recipeTitle: 'Yeast Hot Rolls',
      recipeUrl: 'http://allrecipes.com/recipe/yeast-hot-rolls/detail.aspx',
      recipeImage: 'https://images.media-allrecipes.com/userphotos/591685.jpg',
      recipeIngredients: [
        '3 tablespoons potato flakes',
        '1 ½ cups milk',
        '½ cup vegetable oil',
        '½ cup white sugar',
        '1 teaspoon white sugar',
        '2  eggs',
        '1 teaspoon salt',
        '1 tablespoon active dry yeast',
        '6 ½ cups all-purpose flour',
        '½ cup boiling water',
        '½ cup warm water (110 degrees F/45 degrees C)'
      ],
      sustainabilityScore: 4.512544458040354,
      accuracy: 100
    },
    {
      recipeTitle: 'Chicken Cacciatore',
      recipeUrl: 'http://allrecipes.com/recipe/chicken-cacciatore/detail.aspx',
      recipeImage: 'https://images.media-allrecipes.com/userphotos/8046502.jpg',
      recipeIngredients: [
        '3 slices bacon, chopped',
        '1 (2 to 3 pound) whole chicken, cut into pieces',
        'salt and pepper to taste',
        '1 tablespoon butter',
        '1 large onion, sliced',
        '2 cloves garlic, chopped',
        '1 cup sliced mushrooms',
        '1 cup red wine',
        '2 teaspoons brown sugar',
        '½ cup balsamic vinegar',
        '½ cup baby carrots, sliced',
        '1 tablespoon red wine',
        '2 teaspoons cornstarch'
      ],
      sustainabilityScore: 8.409338321870257,
      accuracy: 92.3076923076923
    },
    {
      recipeTitle: 'Fried Chicken Gizzards',
      recipeUrl: 'http://allrecipes.com/recipe/fried-chicken-gizzards/detail.aspx',
      recipeImage: 'https://images.media-allrecipes.com/userphotos/2639510.jpg',
      recipeIngredients: [
        '1 ½ pounds chicken gizzards',
        '½ cup all-purpose flour',
        '1 ½ tablespoons seasoned salt',
        '1 ½ teaspoons ground black pepper',
        '1 ½ teaspoons garlic powder  (Optional)',
        '2 cups vegetable oil for frying'
      ],
      sustainabilityScore: 11.625337729943904,
      accuracy: 100
    },
    {
      recipeTitle: 'Grandmas Fudge Cake',
      recipeUrl: 'http://allrecipes.com/recipe/grandmas-fudge-cake/detail.aspx',
      recipeImage: 'https://images.media-allrecipes.com/userphotos/31985.jpg',
      recipeIngredients: [
        '1 (18.25 ounce) package chocolate cake mix',
        '1 ½ cups milk',
        '8 tablespoons all-purpose flour',
        '1 ¼ cups butter',
        '5 tablespoons shortening',
        '1 ½ cups white sugar',
        '1 teaspoon vanilla extract',
        '¼ cup unsweetened cocoa powder',
        '1 pinch salt',
        '1 cup white sugar',
        '¼ cup milk',
        '¼ cup butter'
      ],
      sustainabilityScore: 13.299315597133932,
      accuracy: 83.33333333333334
    },
    {
      recipeTitle: 'Curried Microwaved Chicken',
      recipeUrl: 'http://allrecipes.com/recipe/curried-microwaved-chicken/detail.aspx',
      recipeImage: 'https://images.media-allrecipes.com/userphotos/4410650.jpg',
      recipeIngredients: [
        '1  apple - peeled, cored, and chopped',
        '1  onion, chopped',
        '2 tablespoons butter',
        '3 teaspoons curry powder',
        '1 (10.75 ounce) can  condensed cream of mushroom soup',
        '1 cup heavy cream',
        'salt and pepper to taste',
        '8  chicken thighs, cut into bite size pieces',
        '¾ cup fresh sliced mushrooms',
        '1 teaspoon paprika'
      ],
      sustainabilityScore: 8.830716507011962,
      accuracy: 100
    },
    {
      recipeTitle: 'Picnic Sausage Bread',
      recipeUrl: 'http://allrecipes.com/recipe/picnic-sausage-bread/detail.aspx',
      recipeImage: 'https://imagesvc.meredithcorp.io/v3/mm/image?url=https%3A%2F%2Fimages.media-allrecipes.com%2Fuserphotos%2F1028070.jpg',
      recipeIngredients: [
        '1 pound Italian sausage roll (such as Jimmy Dean®)',
        '1 (13.8 ounce) package refrigerated pizza dough (such as Pillsbury®)',
        '2 cups shredded mozzarella cheese',
        '1 tablespoon olive oil, or as needed'
      ],
      sustainabilityScore: 11.963919307250233,
      accuracy: 100
    },
    {
      recipeTitle: 'Baked Scalloped Potatoes',
      recipeUrl: 'http://allrecipes.com/recipe/baked-scalloped-potatoes/detail.aspx',
      recipeImage: 'https://images.media-allrecipes.com/userphotos/183222.jpg',
      recipeIngredients: [
        '6 large peeled, cubed potatoes',
        '1 (10.75 ounce) can  condensed cream of mushroom soup',
        '1 ¼ cups milk',
        '1  onion, diced',
        '½ teaspoon ground black pepper'
      ],
      sustainabilityScore: 0.8726524109867041,
      accuracy: 100
    },
    {
      recipeTitle: 'Adipoli Parathas',
      recipeUrl: 'http://allrecipes.com/recipe/adipoli-parathas/detail.aspx',
      recipeImage: 'https://imagesvc.meredithcorp.io/v3/mm/image?url=https%3A%2F%2Fimages.media-allrecipes.com%2Fuserphotos%2F5287991.jpg',
      recipeIngredients: [
        '2 cups whole wheat flour',
        '¼ cup vegetable oil',
        '⅔ cup water',
        '½ cup vegetable oil',
        '½ teaspoon mustard seeds',
        '1  green chile pepper, chopped',
        '1 (1 1/2 inch) piece ginger, grated',
        '2  onions, peeled and finely chopped',
        '10  fresh curry leaves',
        '½ teaspoon ground turmeric',
        'sea salt to taste',
        '6 ounces uncooked prawns, peeled and deveined',
        '2  eggs, beaten'
      ],
      sustainabilityScore: 4.291074545004758,
      accuracy: 92.3076923076923
    },
    {
      recipeTitle: 'Frosted Apricot Cookies',
      recipeUrl: 'http://allrecipes.com/recipe/frosted-apricot-cookies/detail.aspx',
      recipeImage: 'https://images-gmi-pmc.edge-generalmills.com/24a1a212-b204-427c-bd5c-e9c8bf146504.jpg',
      recipeIngredients: [
        '1 ¼ cups all-purpose flour',
        '¼ cup white sugar',
        '1 ½ teaspoons baking powder',
        '¼ teaspoon salt',
        '½ cup butter, softened',
        '3 ounces cream cheese, softened',
        '½ cup flaked coconut',
        '½ cup apricot preserves'
      ],
      sustainabilityScore: 5.188286576072782,
      accuracy: 100
    },
    {
      recipeTitle: 'Simple Lasagna Burgers',
      recipeUrl: 'http://allrecipes.com/recipe/simple-lasagna-burgers/detail.aspx',
      recipeImage: 'https://images.media-allrecipes.com/userphotos/821067.jpg',
      recipeIngredients: [
        '1 pound ground beef',
        '1 pound bulk Italian sausage',
        '1 tablespoon crushed red pepper flakes',
        '1 tablespoon Italian seasoning',
        '12 ounces ricotta cheese',
        '1 cup spaghetti sauce',
        '⅓ cup grated Parmesan cheese',
        '1 bunch fresh spinach',
        '6  hamburger buns, split'
      ],
      sustainabilityScore: 35.77348885874961,
      accuracy: 77.77777777777779
    },
    {
      recipeTitle: 'Broiled Bbq Pork Chops',
      recipeUrl: 'http://allrecipes.com/recipe/broiled-bbq-pork-chops/detail.aspx',
      recipeImage: 'https://images.media-allrecipes.com/userphotos/985152.jpg',
      recipeIngredients: [
        '1 cup ketchup',
        '1/2 cup water',
        '3 tablespoons distilled white vinegar',
        '2 tablespoons Worcestershire sauce',
        '2 teaspoons brown sugar',
        '1/2 teaspoon salt',
        '1/4 teaspoon ground black pepper',
        '1/2 teaspoon paprika',
        '1/2 teaspoon chili powder',
        '4 pork loin chops, cut about 3/4 inch thick'
      ],
      sustainabilityScore: 3.492621961375,
      accuracy: 90
    },
    {
      recipeTitle: 'Jerusalem Pudding',
      recipeUrl: 'http://allrecipes.com/recipe/jerusalem-pudding/detail.aspx',
      recipeImage: 'http://allrecipes.com/recipe/jerusalem-pudding/detail.aspx',
      recipeIngredients: [
        '½ cup cold water',
        '2 tablespoons uncooked rice',
        '1 cup whipping cream',
        '20  dates, pitted and chopped',
        '½ cup powdered sugar',
        '1 (.25 ounce) envelope unflavored gelatin',
        '¼ cup water',
        '½ teaspoon vanilla extract'
      ],
      sustainabilityScore: 4.61605483734187,
      accuracy: 75
    },
    {
      recipeTitle: 'All American Patty Melts',
      recipeUrl: 'http://allrecipes.com/recipe/all-american-patty-melts/detail.aspx',
      recipeImage: 'https://images.media-allrecipes.com/userphotos/990075.jpg',
      recipeIngredients: [
        '2 teaspoons butter or margarine',
        '1 large onion, thinly sliced',
        '1 pound ground turkey or lean ground beef',
        'Salt and pepper (optional)',
        '8 slices Sargento Deli Syle Sliced American Cheese',
        '8 slices rye or pumpernickel bread',
        'Butter flavor cooking spray'
      ],
      sustainabilityScore: 6.802985189724635,
      accuracy: 85.71428571428571
    },
    {
      recipeTitle: 'Chicken Primavera Alfredo',
      recipeUrl: 'http://allrecipes.com/recipe/chicken-primavera-alfredo/detail.aspx',
      recipeImage: 'https://imagesvc.meredithcorp.io/v3/mm/image?url=https%3A%2F%2Fimages.media-allrecipes.com%2Fuserphotos%2F998735.jpg',
      recipeIngredients: [
        '1 pound fettuccine pasta',
        '¼ cup butter',
        '2  skinless, boneless chicken breast halves, cut into strips',
        '1 clove garlic, minced, or more to taste',
        '4 ounces sliced fresh mushrooms',
        '1  green bell pepper, sliced',
        '1  red bell pepper, sliced',
        '½ cup butter',
        '1 pint heavy whipping cream',
        '½ cup grated Parmesan cheese',
        '1 (8 ounce) package cream cheese, cut into pieces',
        'salt and ground black pepper to taste',
        '1  egg, beaten'
      ],
      sustainabilityScore: 8.858704946111647,
      accuracy: 84.61538461538461
    },
    {
      recipeTitle: 'Star Spangled Burgers',
      recipeUrl: 'http://allrecipes.com/recipe/star-spangled-burgers/detail.aspx',
      recipeImage: 'https://images.media-allrecipes.com/userphotos/934677.jpg',
      recipeIngredients: [
        '2 pounds ground beef',
        '1 large red bell pepper, diced',
        '2 cloves fresh garlic, minced',
        '1 teaspoon salt',
        '1 teaspoon ground black pepper',
        '2 (4 ounce) packages crumbled blue cheese',
        '8  hamburger buns'
      ],
      sustainabilityScore: 43.09400424466,
      accuracy: 100
    },
    {
      recipeTitle: 'Korean Soft Tofu Stew Soon Du Bu Jigae',
      recipeUrl: 'http://allrecipes.com/recipe/korean-soft-tofu-stew-soon-du-bu-jigae/detail.aspx',
      recipeImage: 'https://imagesvc.meredithcorp.io/v3/mm/image?url=https%3A%2F%2Fimages.media-allrecipes.com%2Fuserphotos%2F1194567.jpg',
      recipeIngredients: [
        '1 teaspoon vegetable oil',
        '1 teaspoon Korean chile powder',
        '2 tablespoons ground beef  (Optional)',
        '1 tablespoon Korean soy bean paste (doenjang)',
        '1 cup water',
        'salt and pepper to taste',
        '1 (12 ounce) package Korean soon tofu or soft tofu, drained and sliced',
        '1  egg',
        '1 teaspoon sesame seeds',
        '1  green onion, chopped'
      ],
      sustainabilityScore: 2.152299179386927,
      accuracy: 100
    },
    {
      recipeTitle: 'Korean Bbq Inspired Short Ribs',
      recipeUrl: 'http://allrecipes.com/recipe/korean-bbq-inspired-short-ribs/detail.aspx',
      recipeImage: 'https://images.media-allrecipes.com/userphotos/4877232.jpg',
      recipeIngredients: [
        '¼ cup rice wine vinegar',
        '¼ cup soy sauce',
        '¼ cup sriracha hot sauce, or to taste',
        '2 tablespoons olive oil',
        '¼ cup brown sugar',
        '2  green onions, chopped  (Optional)',
        '3 cloves garlic, minced, or more to taste',
        '1 tablespoon ground black pepper',
        '1 tablespoon toasted sesame seeds',
        '1 tablespoon minced fresh ginger',
        '1 pinch Chinese five-spice powder',
        '2 pounds beef short ribs, cut 1/3-inch-thick across the bones'
      ],
      sustainabilityScore: 42.74127518087955,
      accuracy: 91.66666666666666
    },
    {
      recipeTitle: 'Frosted Apricot Cookies',
      recipeUrl: 'http://allrecipes.com/recipe/frosted-apricot-cookies/detail.aspx',
      recipeImage: 'https://images-gmi-pmc.edge-generalmills.com/24a1a212-b204-427c-bd5c-e9c8bf146504.jpg',
      recipeIngredients: [
        '1 ¼ cups all-purpose flour',
        '¼ cup white sugar',
        '1 ½ teaspoons baking powder',
        '¼ teaspoon salt',
        '½ cup butter, softened',
        '3 ounces cream cheese, softened',
        '½ cup flaked coconut',
        '½ cup apricot preserves'
      ],
      sustainabilityScore: 5.188286576072782,
      accuracy: 100
    },
    {
      recipeTitle: 'Italian Nachos Restaurant Style',
      recipeUrl: 'http://allrecipes.com/recipe/italian-nachos-restaurant-style/detail.aspx',
      recipeImage: 'https://imagesvc.meredithcorp.io/v3/mm/image?url=https%3A%2F%2Fimages.media-allrecipes.com%2Fuserphotos%2F784542.jpg',
      recipeIngredients: [
        '1 pound bulk Italian sausage',
        '7 ounces of tortilla chips',
        '2 ounce sliced pepperoni',
        '½ pound shredded mozzarella cheese',
        '½ cup banana peppers, drained',
        '1 ¼ cups pizza sauce'
      ],
      sustainabilityScore: 11.297256917184926,
      accuracy: 100
    },
    {
      recipeTitle: 'Santas Whiskers Ii',
      recipeUrl: 'http://allrecipes.com/recipe/santas-whiskers-ii/detail.aspx',
      recipeImage: 'http://allrecipes.com/recipe/santas-whiskers-ii/detail.aspx',
      recipeIngredients: [
        '2  egg whites',
        '¼ teaspoon cream of tartar',
        '⅛ teaspoon salt',
        '½ cup white sugar',
        '¾ teaspoon vanilla extract',
        '1 cup chopped walnuts',
        '2 cups semisweet chocolate chips'
      ],
      sustainabilityScore: 4.909664173992333,
      accuracy: 85.71428571428571
    },
    {
      recipeTitle: 'Chocolate Peanut Butter And Bacon Cookies',
      recipeUrl: 'http://allrecipes.com/recipe/chocolate-peanut-butter-and-bacon-cookies/detail.aspx',
      recipeImage: 'https://imagesvc.meredithcorp.io/v3/mm/image?url=https%3A%2F%2Fimages.media-allrecipes.com%2Fuserphotos%2F5272966.jpg',
      recipeIngredients: [
        '1 pound bacon',
        '2 cups all-purpose flour',
        '¾ cup unsweetened cocoa powder',
        '½ teaspoon baking soda',
        '½ cup butter, softened',
        '1 cup brown sugar',
        '¼ cup white sugar',
        '2  large eggs',
        '1 teaspoon vanilla extract',
        '1 (13 ounce) package miniature peanut butter cups, chopped',
        '¼ cup peanut butter'
      ],
      sustainabilityScore: 13.834658605590684,
      accuracy: 72.72727272727273
    },
    {
      recipeTitle: 'Slow Cooker Pulled Pork With Orange Juice',
      recipeUrl: 'http://allrecipes.com/recipe/slow-cooker-pulled-pork-with-orange-juice/detail.aspx',
      recipeImage: 'https://imagesvc.meredithcorp.io/v3/mm/image?url=https%3A%2F%2Fimages.media-allrecipes.com%2Fuserphotos%2F1999238.jpg',
      recipeIngredients: [
        '1 (5 pound) pork butt roast',
        '1 cup water',
        '½ cup orange juice',
        'salt and ground black pepper to taste',
        '1 ½ tablespoons minced garlic',
        '1 tablespoon stone-ground mustard, or more to taste',
        '1 large sweet onion (such as Vidalia®), chopped'
      ],
      sustainabilityScore: 7.026651234693145,
      accuracy: 85.71428571428571
    },
    {
      recipeTitle: 'Roasted Creole Potatoes',
      recipeUrl: 'http://allrecipes.com/recipe/roasted-creole-potatoes/detail.aspx',
      recipeImage: 'https://images.media-allrecipes.com/userphotos/532396.jpg',
      recipeIngredients: [
        '2 pounds cubed white potatoes',
        '12 ounces andouille sausage, sliced',
        '½ cup chopped green bell pepper',
        '½ cup chopped onion',
        '2 tablespoons olive oil',
        '1 tablespoon paprika',
        '1 tablespoon Creole seasoning',
        '1 tablespoon garlic powder',
        '1 teaspoon ground black pepper'
      ],
      sustainabilityScore: 6.176852987960495,
      accuracy: 88.88888888888889
    },
    {
      recipeTitle: 'Cod Au Gratin',
      recipeUrl: 'http://allrecipes.com/recipe/cod-au-gratin/detail.aspx',
      recipeImage: 'https://imagesvc.meredithcorp.io/v3/mm/image?url=https%3A%2F%2Fimages.media-allrecipes.com%2Fuserphotos%2F1007936.jpg',
      recipeIngredients: [
        '2 pounds cod fillets',
        '3 tablespoons margarine',
        '6 tablespoons all-purpose flour',
        '2 cups milk',
        'salt and ground black pepper to taste',
        '1 ½ cups shredded Cheddar cheese'
      ],
      sustainabilityScore: 7.46025262303527,
      accuracy: 100
    },
    {
      recipeTitle: 'Triple Chocolate Chip Cookies',
      recipeUrl: 'http://allrecipes.com/recipe/triple-chocolate-chip-cookies/detail.aspx',
      recipeImage: 'https://imagesvc.meredithcorp.io/v3/mm/image?url=https%3A%2F%2Fimages.media-allrecipes.com%2Fuserphotos%2F605107.jpg&w=595&h=595&c=sc&poi=face&q=85',
      recipeIngredients: [
        '1 ¼ cups unsalted butter',
        '2  eggs',
        '1 teaspoon vanilla extract',
        '1 ¾ cups all-purpose flour',
        '1 teaspoon baking soda',
        '1 teaspoon salt',
        '¾ cup white sugar',
        '¾ cup packed brown sugar',
        '1 ½ cups white chocolate chips',
        '1 ½ cups milk chocolate chips',
        '1 ½ cups semi-sweet chocolate chips',
        '1 cup chopped walnuts'
      ],
      sustainabilityScore: 15.423043692906855,
      accuracy: 83.33333333333334
    },
    {
      recipeTitle: 'Shelias Dip',
      recipeUrl: 'http://allrecipes.com/recipe/shelias-dip/detail.aspx',
      recipeImage: 'http://allrecipes.com/recipe/shelias-dip/detail.aspx',
      recipeIngredients: [
        '2 (8 ounce) packages cream cheese, softened',
        '2 (8 ounce) cans crushed pineapple, drained',
        '½ cup chopped green bell pepper',
        '½ cup chopped red bell pepper',
        '2 teaspoons garlic powder',
        '2 teaspoons seasoned salt',
        '2 tablespoons dried minced onion flakes',
        '1 ½ cups finely chopped pecans'
      ],
      sustainabilityScore: 3.1940338998178572,
      accuracy: 100
    },
    {
      recipeTitle: 'Four Fruit Pie',
      recipeUrl: 'http://allrecipes.com/recipe/four-fruit-pie/detail.aspx',
      recipeImage: 'https://imagesvc.meredithcorp.io/v3/mm/image?url=https%3A%2F%2Fimages.media-allrecipes.com%2Fuserphotos%2F48516.jpg&w=595&h=398&c=sc&poi=face&q=85',
      recipeIngredients: [
        '1 (9 inch) pie shell',
        '3  apples',
        '3  fresh peaches',
        '1  pear',
        '1 cup raspberries',
        '¾ cup white sugar',
        '1 teaspoon ground cinnamon',
        '3 tablespoons all-purpose flour',
        '⅓ cup packed brown sugar',
        '¾ cup all-purpose flour',
        '6 tablespoons butter',
        '½ cup chopped pecans'
      ],
      sustainabilityScore: 4.698740367167947,
      accuracy: 91.66666666666666
    },
    {
      recipeTitle: 'Nancys Chicken In Puff Pastry',
      recipeUrl: 'http://allrecipes.com/recipe/nancys-chicken-in-puff-pastry/detail.aspx',
      recipeImage: 'https://images.media-allrecipes.com/userphotos/8399118.jpg',
      recipeIngredients: [
        '1 tablespoon vegetable oil',
        'salt and pepper to taste',
        '4 (4 ounce) skinless, boneless chicken breast halves',
        '1 (8 ounce) package cream cheese with chives',
        '4  puff pastry shells'
      ],
      sustainabilityScore: 6.263847685985635,
      accuracy: 100
    },
    {
      recipeTitle: 'Shearers Mince And Potato Hot Pot',
      recipeUrl: 'http://allrecipes.com/recipe/shearers-mince-and-potato-hot-pot/detail.aspx',
      recipeImage: 'https://imagesvc.meredithcorp.io/v3/mm/image?url=https%3A%2F%2Fimages.media-allrecipes.com%2Fuserphotos%2F7245041.jpg&w=596&h=596&c=sc&poi=face&q=85',
      recipeIngredients: [
        '5 medium potatoes, peeled and thinly sliced',
        '1 tablespoon olive oil',
        '1 pound ground beef',
        '1  onion, chopped',
        '1 tablespoon tomato sauce',
        '1 tablespoon Worcestershire sauce',
        'salt and pepper to taste',
        '¼ cup butter',
        '¼ cup all-purpose flour',
        '2 cups milk',
        '1 cup shredded sharp Cheddar cheese',
        '1 (6 ounce) can mushrooms, drained',
        '2 tablespoons butter, diced'
      ],
      sustainabilityScore: 27.49182969889927,
      accuracy: 92.3076923076923
    },
    {
      recipeTitle: 'Ukrainian Red Borscht Soup',
      recipeUrl: 'http://allrecipes.com/recipe/ukrainian-red-borscht-soup/detail.aspx',
      recipeImage: 'https://images.media-allrecipes.com/userphotos/6591426.jpg',
      recipeIngredients: [
        '1 (16 ounce) package pork sausage',
        '3 medium beets, peeled and shredded',
        '3  carrots, peeled and shredded',
        '3 medium baking potatoes, peeled and cubed',
        '1 tablespoon vegetable oil',
        '1 medium onion, chopped',
        '1 (6 ounce) can tomato paste',
        '¾ cup water',
        '½ medium head cabbage, cored and shredded',
        '1 (8 ounce) can diced tomatoes, drained',
        '3 cloves garlic, minced',
        'salt and pepper to taste',
        '1 teaspoon white sugar, or to taste',
        '½ cup sour cream, for topping',
        '1 tablespoon chopped fresh parsley for garnish'
      ],
      sustainabilityScore: 3.693806736068021,
      accuracy: 93.33333333333333
    },
    {
      recipeTitle: 'Michelles Peanut Butter Dots',
      recipeUrl: 'http://allrecipes.com/recipe/michelles-peanut-butter-dots/detail.aspx',
      recipeImage: 'https://imagesvc.meredithcorp.io/v3/mm/image?url=https%3A%2F%2Fimages.media-allrecipes.com%2Fuserphotos%2F8092561.jpg',
      recipeIngredients: [
        '1 cup butter',
        '1 cup white sugar',
        '1 cup peanut butter',
        '¼ cup milk',
        '1 teaspoon vanilla extract',
        '2 cups all-purpose flour'
      ],
      sustainabilityScore: 8.851087685836694,
      accuracy: 83.33333333333334
    },
    {
      recipeTitle: 'Aracelys Flan',
      recipeUrl: 'http://allrecipes.com/recipe/aracelys-flan/detail.aspx',
      recipeImage: 'https://images.media-allrecipes.com/userphotos/5392222.jpg',
      recipeIngredients: [
        '10  egg yolks',
        '1 (14 ounce) can sweetened condensed milk',
        '1 (12 ounce) can evaporated milk',
        '3 tablespoons margarine',
        '3 tablespoons brown sugar'
      ],
      sustainabilityScore: 5.565217052699909,
      accuracy: 100
    },
    {
      recipeTitle: 'Sinful Sour Cream Chicken',
      recipeUrl: 'http://allrecipes.com/recipe/sinful-sour-cream-chicken/detail.aspx',
      recipeImage: 'https://imagesvc.meredithcorp.io/v3/mm/image?url=https%3A%2F%2Fimages.media-allrecipes.com%2Fuserphotos%2F1061588.jpg',
      recipeIngredients: [
        '1 tablespoon butter',
        '4  skinless, boneless chicken breast halves',
        '½ cup white wine',
        '½ cup chopped white onion',
        '1 cup sour cream',
        'salt and ground black pepper to taste'
      ],
      sustainabilityScore: 5.297376547550584,
      accuracy: 100
    },
    {
      recipeTitle: 'Mulligatawny Soup I',
      recipeUrl: 'http://allrecipes.com/recipe/mulligatawny-soup-i/detail.aspx',
      recipeImage: 'https://images.media-allrecipes.com/userphotos/4585458.jpg',
      recipeIngredients: [
        '½ cup chopped onion',
        '2 stalks celery, chopped',
        '1  carrot, diced',
        '¼ cup butter',
        '1 ½ tablespoons all-purpose flour',
        '1 ½ teaspoons curry powder',
        '4 cups chicken broth',
        '½  apple, cored and chopped',
        '¼ cup white rice',
        '1  skinless, boneless chicken breast half - cut into cubes',
        'salt to taste',
        'ground black pepper to taste',
        '1 pinch dried thyme',
        '½ cup heavy cream, heated'
      ],
      sustainabilityScore: 12.82426235509973,
      accuracy: 85.71428571428571
    },
    {
      recipeTitle: 'Grandmother Stougaards Caramel Pecan Sweet Rolls',
      recipeUrl: 'http://allrecipes.com/recipe/grandmother-stougaards-caramel-pecan-sweet-rolls/detail.aspx',
      recipeImage: 'https://imagesvc.meredithcorp.io/v3/mm/image?url=https%3A%2F%2Fimages.media-allrecipes.com%2Fuserphotos%2F1093667.jpg',
      recipeIngredients: [
        '½ cup milk',
        '½ teaspoon white sugar',
        '½ teaspoon salt',
        '¼ cup margarine',
        '⅛ cup warm water (110 degrees F)',
        '1 (.25 ounce) envelope active dry yeast',
        '1  egg',
        '2 ½ cups all-purpose flour, or as needed',
        '2 tablespoons margarine, softened',
        '¼ cup white sugar',
        '1 teaspoon ground cinnamon',
        '2 tablespoons light corn syrup',
        '1 ½ tablespoons water',
        '3 tablespoons margarine',
        '¾ cup packed brown sugar',
        '½ cup pecan halves'
      ],
      sustainabilityScore: 3.0256227211722897,
      accuracy: 93.75
    },
    {
      recipeTitle: 'Healthier Crema De Fruta',
      recipeUrl: 'http://allrecipes.com/recipe/healthier-crema-de-fruta/detail.aspx',
      recipeImage: 'http://allrecipes.com/recipe/healthier-crema-de-fruta/detail.aspx',
      recipeIngredients: [
        '1 ½ cups 2% evaporated milk',
        '1 ½ cups 2% milk',
        '¾ cup white sugar',
        '½ cup water',
        '¼ cup all-purpose flour',
        '5  egg yolks',
        '1 ¼ cups water',
        '1 (16 ounce) can fruit cocktail in heavy syrup, syrup drained and reserved',
        '3 (1 ounce) packages unflavored gelatin',
        '2 tablespoons lemon juice, or to taste',
        '2 (5.3 ounce) packages ladyfingers'
      ],
      sustainabilityScore: 3.541827050128917,
      accuracy: 100
    },
    {
      recipeTitle: 'Magic Lemon Pie',
      recipeUrl: 'http://allrecipes.com/recipe/magic-lemon-pie/detail.aspx',
      recipeImage: 'https://images.media-allrecipes.com/userphotos/926111.jpg',
      recipeIngredients: [
        '1 (9 inch) prepared graham cracker crust',
        '½ cup lemon juice',
        '1 (14 ounce) can sweetened condensed milk',
        '2  eggs, separated',
        '¼ teaspoon cream of tartar',
        '4 tablespoons white sugar'
      ],
      sustainabilityScore: 0.7031268803209404,
      accuracy: 100
    },
    {
      recipeTitle: 'Easy Mango Cake',
      recipeUrl: 'http://allrecipes.com/recipe/easy-mango-cake/detail.aspx',
      recipeImage: 'https://images.media-allrecipes.com/userphotos/2262002.jpg',
      recipeIngredients: [
        '1  ripe mango',
        '1 (18.25 ounce) box yellow cake mix',
        '3  eggs',
        '½ cup water',
        '¼ cup vegetable oil',
        '2 tablespoons lime zest',
        '1 tablespoon butter, melted',
        '½  lime, juiced',
        '1 tablespoon golden rum  (Optional)',
        "2 cups confectioners' sugar"
      ],
      sustainabilityScore: 1.6814848808696738,
      accuracy: 80
    },
    {
      recipeTitle: 'Noreens Moms Simple Summer Squash Bake',
      recipeUrl: 'http://allrecipes.com/recipe/noreens-moms-simple-summer-squash-bake/detail.aspx',
      recipeImage: 'https://imagesvc.meredithcorp.io/v3/mm/image?url=https%3A%2F%2Fimages.media-allrecipes.com%2Fuserphotos%2F2319607.jpg',
      recipeIngredients: [
        '3 large yellow summer squash - peeled, seeded, and sliced into 1/2 inch rounds',
        '½ medium onion, cut into 1/2-inch slices',
        '2 tablespoons butter',
        '4 slices Swiss cheese',
        'salt and pepper to taste'
      ],
      sustainabilityScore: 3.702055617758023,
      accuracy: 100
    },
    {
      recipeTitle: 'Mama Gunns Pound Cake',
      recipeUrl: 'http://allrecipes.com/recipe/mama-gunns-pound-cake/detail.aspx',
      recipeImage: 'https://images.media-allrecipes.com/userphotos/1002258.jpg',
      recipeIngredients: [
        '4 cups all-purpose flour',
        '1 teaspoon baking powder',
        '1 teaspoon salt',
        '1 cup butter',
        '1 cup shortening',
        '3 cups white sugar',
        '6  large eggs',
        '¾ cup milk',
        '1 teaspoon lemon extract',
        '1 teaspoon vanilla extract'
      ],
      sustainabilityScore: 12.384113713954799,
      accuracy: 90
    },
    {
      recipeTitle: 'Quick Thai Fried Bananas',
      recipeUrl: 'http://allrecipes.com/recipe/quick-thai-fried-bananas/detail.aspx',
      recipeImage: 'https://imagesvc.meredithcorp.io/v3/mm/image?url=https%3A%2F%2Fimages.media-allrecipes.com%2Fuserphotos%2F1944153.jpg',
      recipeIngredients: [
        '2 tablespoons butter',
        '4  firm bananas, peeled and cut into 1- to 2-inch pieces',
        '¼ cup brown sugar',
        '½ teaspoon ground cinnamon',
        '1  lime, juiced',
        '1 teaspoon black sesame seeds'
      ],
      sustainabilityScore: 1.558001546054946,
      accuracy: 83.33333333333334
    },
    {
      recipeTitle: 'Crab Stuffed Flounder',
      recipeUrl: 'http://allrecipes.com/recipe/crab-stuffed-flounder/detail.aspx',
      recipeImage: 'https://imagesvc.meredithcorp.io/v3/mm/image?url=https%3A%2F%2Fimages.media-allrecipes.com%2Fuserphotos%2F4548952.jpg',
      recipeIngredients: [
        '1 ½ pounds flounder fillets',
        '1 cup crabmeat - drained, flaked and cartilage removed',
        '1 tablespoon finely chopped green bell pepper',
        '¼ teaspoon ground dry mustard',
        '¼ teaspoon Worcestershire sauce',
        '¼ teaspoon salt',
        'ground white pepper, to taste',
        '3  crushed saltine crackers',
        '1  egg white',
        '1 tablespoon mayonnaise',
        '¼ cup butter, melted',
        '1  egg yolk',
        '5 tablespoons mayonnaise',
        '½ teaspoon paprika',
        '1 tablespoon dried parsley'
      ],
      sustainabilityScore: 5.287716341256223,
      accuracy: 80
    },
    {
      recipeTitle: 'Cappuccino Muffins With Chocolate And Cranberries',
      recipeUrl: 'http://allrecipes.com/recipe/cappuccino-muffins-with-chocolate-and-cranberries/detail.aspx',
      recipeImage: 'https://images.media-allrecipes.com/userphotos/936620.jpg',
      recipeIngredients: [
        '2 cups sifted all-purpose flour',
        '1 tablespoon baking powder',
        '1 teaspoon baking soda',
        '½ cup cocoa powder',
        '½ teaspoon salt',
        '½ cup semisweet chocolate chips',
        '½ cup dried cranberries',
        '1 teaspoon minced orange zest',
        '½ cup butter, softened',
        '½ cup white sugar',
        '½ cup brown sugar',
        '1 cup sour cream',
        '¾ cup milk',
        '2  eggs',
        '2 tablespoons instant coffee granules',
        '¼ cup water'
      ],
      sustainabilityScore: 10.027215180799805,
      accuracy: 93.75
    }
  ]
  const ratio = 13.2993155 / 10 // Ceiling for normalization, divided by the steps wanted in the normalization. Scores higher than 10 will still just be 10.
  const recipePoolWithNormalizedScores = selectedRecipesForSurvey.map(v => ({...v, normalizedScore: Math.min(Math.round(v.sustainabilityScore/ratio), 10)}));
  const possibleInputModes = ['discreete', 'slider'];
  const randomNumber = Math.floor(Math.random()*possibleInputModes.length);
  const selectedInputMode = possibleInputModes[randomNumber]

  //Application State
  const [mode, setMode] = useState("Intro"); //Dictates which step the user is currently at.
  const [isHidden, setIsHidden] = useState(false); // Hides submit survey button upon click
  const [sessionId, setSessionId] = useState(uuidv4()); //Generate session ID
  const [ratingsGiven, setRatingsGiven] = useState([]); //List of all the recipes shown to the user and what rating they gave it.
  const [timeSpent, setTimeSpent] = useState(null); //The total amount of time that the user has spent on the survey.
  const [inputMode, setInputMode] = useState(selectedInputMode); // ("discreete" or "slider")
  const [inputGranularity, setInputGranularity] = useState("GHGEOnly"); // ("GHGHEonly" or "GHGEandLU")
  const [demographicsData, setDemoGraphicsData] = useState(null);
  const [recipePool, setRecipePool] = useState(recipePoolWithNormalizedScores);

  //When a user clicks a recipe button, append the recipe/rating object to ratingsGiven state.
  const handleRatingGiven = (object) => {
    setRatingsGiven((previousRatings) => {
      return [...previousRatings, { object }]; //Append the newest rated recipe to list of rated recipes.
    });
  };

  const handleDemographicsSubmitted = (object) => {
    setDemoGraphicsData(object)
  }



  const postSurveyData = () => {
    const url =
      "https://us-east-1.aws.webhooks.mongodb-realm.com/api/client/v2.0/app/survey-thesis-wfrzf/service/myHttp/incoming_webhook/postdata?secret=asd123asd";

    const body = {
      surveydata: {
        demographics: demographicsData,
        sessionId: sessionId,
        timeSpent: timeSpent,
        recipesRated: ratingsGiven,
        inputGranularity: inputGranularity,
        inputMode: inputMode
      }
    };

    axios
      .post(url, body)
      .then(function (response) {
        setIsHidden(true);
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  const SubmitButton = () => {
    if (isHidden === false) {
      return (
        <Button
          style={{ display: "flex", margin: "auto" }}
          onClick={() => postSurveyData()}
        >
          Submit
        </Button>
      );
    }
    if (isHidden === true) {
      return (
        <>
          <Confetti
            width={window.innerWidth}
            height={window.innerHeight}
            recycle={false}
          />
          <Button disabled style={{ display: "flex", margin: "auto" }}>
            Survey Submitted
          </Button>
        </>
      );
    }
  };

  // Shuffle the recipe pool and send 10 random recipes to the survey component.
  const recipesToShow = 10;
  const shuffled = recipePool.sort(() => 0.5 - Math.random());
  let ten_random = shuffled.slice(0, recipesToShow + 1);

  if (mode === "Intro") {
    return (
      <>
        <SurveyStart onClick={(value) => setMode(value)}></SurveyStart>
      </>
    );
  }
  if (mode === "SurveyStart") {
    return (
      <>
        <Timer setTimeSpent={(time) => setTimeSpent(time)} />
        <BasicQuestionCard
          inputMode={inputMode}
          inputGranularity={inputGranularity}
          recipes={ten_random}
          setUsersRatingInput={(recipe) => handleRatingGiven(recipe)}
          setUsersDemographicData={(demographics) => handleDemographicsSubmitted(demographics)}
          onClick={(value) => setMode(value)}
        />
      </>
    );
  }
  if (mode === "Fin") {
    return (
      <>
        <h1 style={{ textAlign: "center", marginTop: "100px" }}>
          Thank you for participating in the survey!{" "}
        </h1>
        <p style={{ textAlign: "center", marginTop: "10px" }}>
          Click the button below to submit your answers to the server.
        </p>
        <SubmitButton />
      </>
    );
  }
}

export default App;
